# Card Holder

Simple project for card holding. 

## Screenshots

![image](https://gitlab.com/tokyjo/card-holder/-/raw/main/static/main.jpg)

![image](https://gitlab.com/tokyjo/card-holder/-/raw/main/static/context_with_color.jpg)

![image](https://gitlab.com/tokyjo/card-holder/-/raw/main/static/context_menu.jpg)

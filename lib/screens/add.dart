import 'dart:math';

import 'package:card_holder/data/card.dart';
import 'package:flutter/material.dart' hide Card;
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:hive/hive.dart';

class AddNewCardScreen extends StatefulWidget {
  static const route = "/AddNewCardScreen";
  const AddNewCardScreen({Key? key}) : super(key: key);

  @override
  _AddNewCardScreenState createState() => _AddNewCardScreenState();
}

class _AddNewCardScreenState extends State<AddNewCardScreen> {
  final TextEditingController textEditingControllerCode =
      TextEditingController();
  final TextEditingController textEditingController = TextEditingController();
  @override
  initState() {
    super.initState();
    showScanner();
  }

  showScanner() async {
    String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        "#ff6666", "Cancel", false, ScanMode.DEFAULT);
    if (barcodeScanRes != '-1') {
      setState(() {
        textEditingControllerCode.text = barcodeScanRes;
      });
    }
  }

  _addToBox() async {
    if (textEditingControllerCode.text.trim().isNotEmpty &&
        textEditingController.text.trim().isNotEmpty) {
      final _random = Random();
      final _anotherRandomColor = Color.fromRGBO(
          _random.nextInt(256), _random.nextInt(256), _random.nextInt(256), 1);
      await Hive.box<Card>('cards').add(Card(
          textEditingController.text.trim(),
          textEditingControllerCode.text.trim(),
          _anotherRandomColor.value,
          false,
          null,
          false));
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add new card'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: textEditingControllerCode,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), hintText: 'Card number'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: textEditingController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), hintText: 'Card name'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                    onPressed: _addToBox, child: const Text('Submit')),
              )
            ],
          ),
        ),
      ),
    );
  }
}
